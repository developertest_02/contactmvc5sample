﻿using Sample.Web.Models;
using Sample.Web.SampleData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sample.Web.Helpers
{

    /// <summary>
    /// Maps Entity Framework objects to and from view models
    /// </summary>
    public static class Mapping
    {
        public static ContactViewModel ToModel(this Contact contact)
        {
            var result = new ContactViewModel();
            result.Id = contact.Id;
            result.FirstName = contact.FirstName;
            result.LastName = contact.LastName;
            result.EmailAddress = contact.EmailAddress;
            return result;
        }

        public static Contact FromModel(this ContactViewModel model)
        {
            var result = new Contact();
            if (model.Id.HasValue)
                result.Id = model.Id.Value;
            result.FirstName = model.FirstName;
            result.LastName = model.LastName;
            result.EmailAddress = model.EmailAddress;
            return result;
        }
    }
}