﻿//consistent notifications
function NotificationService() {
    this.showError = function (msg) {
        $.toast({
            heading: 'Error',
            text: msg,
            showHideTransition: 'slide',
            icon: 'error',
            loader: true
        });
    };
    this.showSuccess = function (msg, header) {
        $.toast({
            heading: header,
            text: msg,
            showHideTransition: 'slide',
            icon: 'success'
        });
    };
    this.showInfo = function (msg, header) {
        $.toast({
            heading: header,
            text: msg,
            showHideTransition: 'slide',
            icon: 'info'
        });
    };
}