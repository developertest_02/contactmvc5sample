﻿//handles all rest requests, returns promises
function DataService(opts) {
    var self = this;
    self.defaults = {
        apiBaseUrl: '/api/'
    };
    self.options = $.extend(self.defaults, opts);
    var get = function (ctrl) {
        return $.get(ctrl).fail(function(xhr){            
            console.log('Get Failed');
            $(self).triggerHandler("error", { xhr:xhr });
        });
    };
    var post = function (ctrl, data) {
        return $.post(ctrl, data).fail(function (xhr) {
            console.log('Save Failed');
            $(self).triggerHandler("error", { xhr: xhr });
        });
    };
    var put = function (ctrl, data) {
        return $.ajax({
            type: "PUT",
            url: ctrl,
            data: data,
            dataType: 'json'
        }).fail(function (xhr) {
            console.log('Update Failed');
            $(self).triggerHandler("error", { xhr: xhr });
        });
    };
    var del = function (ctrl) {
        return $.ajax({
            type: "DELETE",
            url: ctrl,
            dataType: 'json'
        }).fail(function (xhr) {
            console.log('Delete Failed');
            $(self).triggerHandler("error", { xhr: xhr });
        });
    };
    this.addContact = function (contact) {
        return post(self.options.apiBaseUrl + 'contact', contact);
    };
    this.deleteContact = function (contact) {
        return del(self.options.apiBaseUrl + 'contact' + '/' + contact.id);
    };
    this.getContacts = function () {
        //return promise
        return get(self.options.apiBaseUrl+'contact');
    };
    this.getNewContact = function () {
        return get(self.options.apiBaseUrl + 'contact/id');
    };
    this.updateContact = function (contact) {
        return put(self.options.apiBaseUrl + 'contact' + '/' + contact.id, contact);
    };
 
}