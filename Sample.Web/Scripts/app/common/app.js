﻿//This is set up like a spa application,
//certain services need to be bootstrapped
function App() {
    var self = this;    
    this.init = function () {
        self.dataService = new DataService();
        self.notificationService = new NotificationService();
        //we show an error on all data service error events
        $(self.dataService).bind("error", function(e, a){
            var message = 'Data Service Error Has Occurred';
            if (a.xhr)
                message += ' ' + a.xhr.statusText;
            self.notificationService.showError(message);
        });

    };
}