﻿//this is a sample of a per page script, it might be a spa in 
//which it is the only page loaded, but none the less, this will live 
//in our page lifecycle
var app;
$(function () {

    app = new App();
    app.init();

    var contactEditModel;
    var contactList;
    //uncouple selectors in html from js that relies on selectors
    //if html attributes change, everything we need to change as far
    //as selectors are concerned are defined below.
    var selectors = {
        contactEditContainer:'#contactEdit',
        contactListContainer: '#contactList',
        contactListItemContainer: '#contactList tbody', 
        newContactHeader: '#newContactHeader',
        contactForm: {
            form:'#contactForm',
            inputFirstName: '#contactForm input[name="firstName"]',
            inputLastName: '#contactForm input[name="lastName"]',
            inputEmailAddress: '#contactForm input[name="emailAddress"]'
        }

    };
    //event wiring
    //we use data action attributes in case we want to swicth, buttons, links, tags etc...
    //as long as attribute is on element, events will wire up
    $('[data-action="addNewContact"]').click(onAddNewContact);
    $('[data-action="contactEditCancel"]').click(onContactEditCancel);
    $('[data-action="saveContact"]').click(onSaveContact);
    $('body').on('click', 'a[data-action="editContact"]', function (e, a) {
        var contactId= $(e.currentTarget).data().id;
        onEditContact(contactId);
    });
    $('body').on('click', 'a[data-action="deleteContact"]', function (e, a) {
        var contactId = $(e.currentTarget).data().id;
        onDeleteContact(contactId);
    });

    //utilities


    //adds new contact row to table passing contact single object
    function addContactRow(contact) {
        var tpl = contact.id ? '<tr data-contact-item-id="'+contact.id+'">':'<tr>';
        tpl += '<td><span>' + contact.firstName + '<span></td>';
        tpl += '<td><span>' + contact.lastName + '<span></td>';
        tpl += '<td><span>' + contact.emailAddress + '<span></td>';
        tpl += '<td><a href="javascript:void(0)" data-action="editContact" data-id="' + contact.id + '">Edit</a></td>';
        tpl += '<td><a href="javascript:void(0)" data-action="deleteContact" data-id="' + contact.id + '">Delete</a></td>';
        tpl += '</tr>';
        $(selectors.contactListItemContainer).append($(tpl));
    }
    //binds form values from model
    function bindContactFromForm() {
        contactEditModel.firstName = $(selectors.contactForm.inputFirstName).val();
        contactEditModel.lastName = $(selectors.contactForm.inputLastName).val();
        contactEditModel.emailAddress = $(selectors.contactForm.inputEmailAddress).val();
    }
    //binds a model to form
    function bindContactToForm(target) {
        //create a clone for purposes of reverting
        contactEditModel = JSON.parse(JSON.stringify(target));
        $(selectors.contactForm.inputFirstName).val(contactEditModel.firstName);
        $(selectors.contactForm.inputLastName).val(contactEditModel.lastName);
        $(selectors.contactForm.inputEmailAddress).val(contactEditModel.emailAddress);
    }
    //resets our contact form
    function unBindContact() {
        contactEditModel = null;
        $(selectors.contactForm.inputFirstName).val(null);
        $(selectors.contactForm.inputLastName).val(null);
        $(selectors.contactForm.inputEmailAddress).val(null);
        $(selectors.contactForm.form).validate().resetForm();
        
    }
    // end utilities

    //event handlers
    function onAddNewContact() {        
        //get a new view model 
        app.dataService.getNewContact().done(function (data, status, xhr) {
            //set current edit model variable to data from server
            contactEditModel = data;
            $(selectors.contactEditContainer).removeClass('hidden');
        });        
    }

    function onContactEditCancel() {
        //set contactViewModel object null
        unBindContact();
        //hide our form since we canceled
        $(selectors.contactEditContainer).addClass('hidden');
    }
    function onDeleteContact(id) {
        //call our crud method
        deleteContact(id);
    }
    function onEditContact(id) {
        //get the item in the list to edit
        var target = _.filter(contactList, function (i) { return i.id === id; })[0];
        //bind the items values to edit form
        bindContactToForm(target);
        //show editor
        $(selectors.contactEditContainer).removeClass('hidden');
    }
    function onSaveContact() {
        var valid = $(selectors.contactForm.form).valid();
        if (valid) {
            //new contact
            if (!contactEditModel.id) {
                addContact();
            } else {
                //we have an id so we should exist
                updateContact();
            }
        }
    }
    //end event handlers

    //CRUD Operations
    function addContact() {   
        //get our form values
        bindContactFromForm();
        app.dataService.addContact(contactEditModel).done(function (data, status, xhr) {
            //create clone, assign new id, add item to list
            contactEditModel.id = data;            
            var newModel = JSON.parse(JSON.stringify(contactEditModel));
            addContactRow(newModel);
            contactList.push(newModel);
            //reset form and object
            onContactEditCancel();
            app.notificationService.showSuccess('New Contact Successfully Added', 'Success');
        });
    }
    function deleteContact(id) {
   
        var contact = _.filter(contactList, function (i) { return i.id === id; })[0];
        app.dataService.deleteContact(contact).done(function (data, status, xhr) {
            contactList.splice(contactList.indexOf(contact, 1));
            $('[data-contact-item-id="' + contact.id + '"').remove();
            app.notificationService.showSuccess('Contact Deleted', 'Success');
        });
    }
    function getContacts() {
        app.dataService.getContacts()
            .done(function (data, status, xhr) {
                contactList = data;
                for (var i = 0; i < contactList.length; i++) {
                    addContactRow(contactList[i]);
                }
                $(selectors.contactListContainer).removeClass('hidden');
            });
    }
    function updateContact() {
        bindContactFromForm();
        app.dataService.updateContact(contactEditModel).done(function (data, status, xhr) {
            onContactEditCancel();
            app.notificationService.showSuccess('Contact Successfully Updated', 'Success');
        });
    }
    //calls data service to get contacts, iterates adding to table   
    getContacts();


});