﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sample.Web.Models
{
    public class ContactViewModel
    {
        //will be null if not persisted
        public int? Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }


    }
}