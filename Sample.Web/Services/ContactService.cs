﻿using Sample.Web.Helpers;
using Sample.Web.Models;
using Sample.Web.SampleData;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sample.Web.Services
{
    /// <summary>
    /// Handles all contact operations from client to persistence and back
    /// implements disposable so one context can be used per request
    /// the need is not exemplified in this trivial sample however.
    /// </summary>
    public class ContactService : IDisposable
    {
        private readonly SampleEntities _db;
        //allow sharing of single context per request
        public ContactService(SampleEntities db)
        {
            _db = db;
        }

        public ContactViewModel GetContact()
        {
            var result = new ContactViewModel();
            return result;
        }
        public async Task<ContactViewModel> GetContact(int id)
        {
            ContactViewModel result = null;
            var contact = await _db.Contacts.SingleOrDefaultAsync(x => x.Id == id);
            if(contact != null)
            {
                result = contact.ToModel();
            }
            return result;
        }
        public async Task<IEnumerable<ContactViewModel>> GetContacts()
        {
            //initialize result, something should always be returned if not exception
            var result = new List<ContactViewModel>();
            var contacts = await _db.Contacts.ToListAsync();
            contacts.ForEach(contact => {
                result.Add(contact.ToModel());
            });
            return result;
        }

        public async Task<int> AddContact(ContactViewModel model)
        {
            
            var contact = model.FromModel();
            _db.Contacts.Add(contact);
            await _db.SaveChangesAsync();
            var result = contact.Id;
            return result;

        }

        public async Task UpdateContact(ContactViewModel model)
        {
            var id = model.Id.GetValueOrDefault();
            var contact = await _db.Contacts.SingleAsync(x => x.Id == model.Id.Value);
            contact.FirstName = model.FirstName;
            contact.LastName = model.LastName;
            contact.EmailAddress = model.EmailAddress;
            await _db.SaveChangesAsync();

        }

        public async Task DeleteContact(int id)
        {
            var contact = await _db.Contacts.SingleAsync(x => x.Id == id);
            _db.Contacts.Remove(contact);
            await _db.SaveChangesAsync();
        }
        public void Dispose(bool disposing)
        {

            if (disposing)
            {
                _db.Dispose();
            }

        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}