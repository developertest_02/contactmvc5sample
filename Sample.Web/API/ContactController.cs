﻿using Sample.Web.Models;
using Sample.Web.SampleData;
using Sample.Web.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Sample.Web.API
{
    public class ContactController : ApiController
    {
        private readonly ContactService _service = new ContactService(new SampleEntities());
        // GET: api/Contact
        [ResponseType(typeof(List<Contact>))]
        public async Task<IHttpActionResult> Get()
        {
            List<Contact> result;
            using (var ctx = new SampleEntities())
            {
                result = await ctx.Contacts.ToListAsync();

            }
            return Ok(result);

        }

        // GET: api/Contact/5
        [ResponseType(typeof(Contact))]
        public async Task<IHttpActionResult> Get(int? id)
        {
            IHttpActionResult result = null;
            ContactViewModel contact = null;
            if (id.HasValue)
            {
                contact = await _service.GetContact(id.Value);
                if (contact == null)
                    result = NotFound();
            }
            else
            {
                contact = _service.GetContact();
                result = Ok(contact);
            }
            return result;

        }

        // POST: api/Contact
        [ResponseType(typeof(int))]
        public async Task<IHttpActionResult> Post([FromBody]ContactViewModel model)
        {
            var id = await _service.AddContact(model);
            IHttpActionResult result;
            try
            {
                result = Ok(id);
            }catch(System.Exception ex)
            {
                result = InternalServerError(ex);
            }
            return result;
        }

        // PUT: api/Contact/5
        public async Task<IHttpActionResult> Put(int id, [FromBody]ContactViewModel model)
        {
            IHttpActionResult result;
            try
            {
                await _service.UpdateContact(model);
                result = Ok(model.Id);
            }catch(System.Exception ex)
            {
                result = InternalServerError(ex);
            }
            return result;
        }

        // DELETE: api/Contact/5
        public async Task<IHttpActionResult> Delete(int id)
        {
            IHttpActionResult result;
            try
            {
                await _service.DeleteContact(id);
                result = Ok(id);
            }
            catch (System.Exception ex)
            {
                result = InternalServerError(ex);
            }
            return result;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _service.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
