# Sample Web
Sample MVC 5 Application Utilizing.

  - JQuery
  - Entity Framework
  - WebApi

### Purpose of Sample Illustrates the Following

  - My typical pattern of client, middle tier, persistence strategy using an example Contact list
  - My Standard WebApi Controller Pattern
  - My Standard Service (Business Logic Layer)
  - Separation of concerns with client side JS code
### Point of Interest
### Middle Tier
##### Sample.Web.API.ContactController
This controller is designed to support a common REST Api. All basic operations that would be expected are operations are supported. Depending on how the API is expected to be consumed, I might design it differently. Perhaps versioning would be included. Sometimes if the API is supposed to support a single application, I may pass objects back and forth instead of using **IHttpActionResult** as a return type.

##### Sample.Web.Services.ContactService
For most unrelated aspects of an application (ie. Contact, List of Log Messages, Product, Shopping Cart) I use a service class. The service class is responsible for handling view models between the client and persistence layer. Seldom does a relational database entity stand alone. It would be nice if we could design a relational database and just scafold everything and pass from the client to the server. I find this never happens. In this example, a Contact might have a collection of addresses, addresses of different types might be applicable. Adding an address might present the user with a list of US States, some users may be Administators and some things we might want to be able to edit with one user, others, not so much. I use a service layer to glue all of this together for the sole purpose of consumption by the client.

In most non-trivial applications, its not only the client that touches the models. There may be scheduled batch processes, entities may be used in entirely different contexts, but common things must happen like validation, business logic, things that apply to all tiers. Its not unusual that my Web Application Layer service classes may call more general classes to facilitate operations, such as **System.Data.Contact.Service** (not included in sample). All new contacts may need to be validated, not just ones coming from a WebApi. Idea is generally, Web Application service layer classes is where I like to put Data -> View Model and View Model->Data.

### Presentation

#### app/common/dataService.js
Applications typically all read and write data. Higher level objects do not care where the data comes from, or how its written. The sample application is extremely simple, however most real world applications are not. Some data may come from one API, some from another, some might come from third party APIs. 

The biggest reasons I like to separate GET,POST,PUT,DELETE operations from higher level page controllers are as follows
- Structure and location of data is constantly changing. The vast majority of application code involves reading and writing data. If fetching and saving data is peppered all over an application a simple API change can wreak havoc in 10000 different js files.
- Many things go wrong with data, we have rules, validation, user input, a buffet of trouble. Having all calls in one place allows for better messaging to the end user (the familliar Oops message when something goes wrong in the API). It also allows for better troubleshooting, our familliar message allows us to quickly see whether someting related to the API was at fault.
- Foremost, **TESTING**. End to End client testing is hard enough without having to mix a DOM in, when we are trying to test our API from the client end. If a data service resides standalone, we can easily mock it, and/or write concise methods to test our API, and create systems tests that can easily be run to assure that our controllers and such, get the data they expect.

#### app/home_index.js
The architecture of this sample does not take into heavily into consideration, whether it is single page, or multi page. This file however assumes a multi-page (load) app. This js file is the driver of such a page. The biggest struggle I have encountered when using JS frameworks that depend on a DOM structure, is that HTML is CONSTANTLY changing. This file illustrates my attempt at making the JS code as agnostic as possible of the HTML. We may change a link to a button, we may change an ordered list to a table, so many things, but the main purpose of this design is to insulate as much as possible HTML from javascript which has expectations. This is achieved by two means, data-attributes which are agnostic to tags, and where that is not always a 100% solution, declaring the selectors required by the JS in a single object per page. If the structure of the Home.aspx page changes, it should have minimal impact on the behavior of the JQuery.

#### app/common/app.js
Whether the application is simgle or multipage, there are certain features that are so common to every feature, that they are almost always needed. Such would include a datasevice, a notification service, etc... The App.js file bootstraps the most common of things. Having a bootstraping js routine allows for the following.
- We do not have to declare and initialize common things in every page controller.
- We can centralize settings (although not demonstrated in this simple app) properties such as baseUrl, whether to output errors to console, log, or both, can be maintained in a single place.
- Testing. Having a bootstraping routine and object allows us to mock it. We can swap out say our dataService, in a single line of code, change virtually any setting that will affect the behavior of the application.
